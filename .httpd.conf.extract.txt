# Apache httpd.conf extract providing URL routing and other directives

#   Example usage in httpd.conf:
#   
#   # Online atlas
#   Include /path/to/online-atlas/.httpd.conf.extract.txt
#   Use MacroPopulationsPast "/public-url/populationspast" "/path/to/populationspast" "/path/to/online-atlas" "/path/to/online-atlas/:/path/to/smarty/:/path/to/php/libraries/"


<Macro MacroPopulationsPast $applicationBaseUrl $localRepoPath $baseRepoPath $includePath>
	
	# Load base codebase
	Include $baseRepoPath/.httpd.conf.extract.txt
	Use MacroOnlineAtlas "$applicationBaseUrl" "$localRepoPath" "$baseRepoPath" "Populations Past" "$includePath"
	
</Macro>
